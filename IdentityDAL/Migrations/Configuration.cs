namespace IdentityDAL.Migrations
{
    using IdentityDAL.Entities;
    using IdentityDAL.Managers;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<IdentityDAL.UserDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(IdentityDAL.UserDbContext context)
        {
            var roles = new List<ApplicationRole>
            {
                new ApplicationRole { Name = "Owner" },
                new ApplicationRole { Name = "Admin" },
                new ApplicationRole { Name = "Client" }
            };

            roles.ForEach(x => context.Roles.AddOrUpdate(r => r.Name, x));
            context.SaveChanges();
            
            var owner = new ApplicationUser { Email = "chief@epum.com", UserName = "CEO" };
            var Roman_admin1 = new ApplicationUser { Email = "Roman@epum.com", UserName = "Roman" };
            var Lena_admin2 = new ApplicationUser { Email = "Lena@epum.com", UserName = "Lena" };
            var Oleh_admin3 = new ApplicationUser { Email = "Oleh@epum.com", UserName = "Oleh" };
            var Rostislav_client1 = new ApplicationUser { Email = "Rostislav@epum.com", UserName = "Rostislav" };
            var Alina_client2 = new ApplicationUser { Email = "Alina@epum.com", UserName = "Alina" };
            var Vitaliy_client3 = new ApplicationUser { Email = "Vitaliy@epum.com", UserName = "Vitaliy" };

            var users = new List<ApplicationUser>()
            {
                owner,
                Roman_admin1, Lena_admin2, Oleh_admin3,
                Rostislav_client1, Alina_client2, Vitaliy_client3
            };

            var userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(context));

            for (int i = 0; i < users.Count; i++)
            {
                userManager.CreateAsync(users[i], $"Qwerty_{i}").GetAwaiter().GetResult();
            }
            
            userManager.AddToRolesAsync(owner.Id, new string[3] { "Client", "Admin", "Owner" }).GetAwaiter().GetResult();

            userManager.AddToRoleAsync(Roman_admin1.Id, "Admin").GetAwaiter().GetResult();
            userManager.AddToRoleAsync(Lena_admin2.Id, "Admin").GetAwaiter().GetResult();
            userManager.AddToRoleAsync(Oleh_admin3.Id, "Admin").GetAwaiter().GetResult();

            userManager.AddToRoleAsync(Rostislav_client1.Id, "Client").GetAwaiter().GetResult();
            userManager.AddToRoleAsync(Alina_client2.Id, "Client").GetAwaiter().GetResult();
            userManager.AddToRoleAsync(Vitaliy_client3.Id, "Client").GetAwaiter().GetResult();

            context.SaveChanges();
        }
    }
}
