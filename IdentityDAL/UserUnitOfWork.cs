﻿using IdentityDAL.Entities;
using IdentityDAL.Interfaces;
using IdentityDAL.Managers;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IdentityDAL
{
    public class UserUnitOfWork : IUserUnitOfWork
    {
        private readonly UserDbContext _context;

        public UserUnitOfWork(UserDbContext context) => _context = context;

        public ApplicationUserManager UserManager => new ApplicationUserManager(new UserStore<ApplicationUser>(_context));

        public ApplicationRoleManager RoleManager => new ApplicationRoleManager(new RoleStore<ApplicationRole>(_context));

        public async Task<int> CommitAsync() => await _context.SaveChangesAsync();

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        private bool _disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
                _disposed = true;
            }
        }
    }
}
