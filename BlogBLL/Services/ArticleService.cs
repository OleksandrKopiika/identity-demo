﻿using AutoMapper;
using BlogBLL.DTOs;
using BlogBLL.Services.Interfaces;
using BlogDAL.Entities;
using BlogDAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogBLL.Services
{
    public class ArticleService : IArticleService
    {
        private readonly IBlogUnitOfWork _blogUnitOfWork;
        private readonly IMapper _mapper;

        public ArticleService(IBlogUnitOfWork blogUnitOfWork, IMapper mapper)
        {
            _blogUnitOfWork = blogUnitOfWork;
            _mapper = mapper;
        }

        public async Task<int> AddAsync(ArticleDto model)
        {
            var article = _mapper.Map<Article>(model);
            article.Slug = article.Title.GenerateSlug();
            article.CreatedAt = DateTime.Now;
            article.UpdatedAt = article.CreatedAt;

            _blogUnitOfWork.ArticleRepository.Add(article);
            await _blogUnitOfWork.CommitAsync();

            return article.Id;
        }

        public async Task UpdateAsync(ArticleDto model)
        {
            var article = await _blogUnitOfWork.ArticleRepository.GetByIdAsync(model.Id);

            article.Title = model.Title;
            article.Slug = article.Title.GenerateSlug();
            article.Text = model.Text;
            article.UpdatedAt = DateTime.Now;
            
            _blogUnitOfWork.ArticleRepository.Update(article);
            await _blogUnitOfWork.CommitAsync();
        }

        public async Task DeleteByIdAsync(int id)
        {
            await _blogUnitOfWork.ArticleRepository.DeleteByIdAsync(id);
            await _blogUnitOfWork.CommitAsync();
        }

        public IEnumerable<ArticleDto> GetAll()
        {
            var articles = _blogUnitOfWork.ArticleRepository.FindAll();

            foreach (var article in articles)
            {
                article.Text = article.Text.Length > 254 ? $"{article.Text.Substring(0, 254)}..." : $"{article.Text}...";
            }

            return _mapper.Map<IEnumerable<Article>, IEnumerable<ArticleDto>>(articles);
        }

        public IEnumerable<ArticleDto> GetArticlesByUserId(Guid userId)
        {
            return _blogUnitOfWork.ArticleRepository
                .GetArticlesByUserId(userId)
                .Select(x => _mapper.Map<ArticleDto>(x));
        }

        public async Task<ArticleDto> GetByIdAsync(int id)
        {
            return _mapper.Map<ArticleDto>(await _blogUnitOfWork.ArticleRepository.GetByIdAsync(id));
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        private bool _disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _blogUnitOfWork.Dispose();
                }
                _disposed = true;
            }
        }
    }
}
