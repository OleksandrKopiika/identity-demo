﻿using BlogBLL.DTOs.Identity;
using BlogBLL.Exceptions;
using BlogBLL.Services.Interfaces.Identity;
using IdentityDAL.Entities;
using IdentityDAL.Interfaces;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace BlogBLL.Services.Identity
{
    public class UserService : IUserService
    {
        private readonly IUserUnitOfWork _userUnitOfWork;

        public UserService(IUserUnitOfWork userUnitOfWork)
        {
            _userUnitOfWork = userUnitOfWork;
        }

        public async Task Create(UserDto userDto)
        {
            var user = new ApplicationUser { Email = userDto.Email, UserName = userDto.Email };
            var result = await _userUnitOfWork.UserManager.CreateAsync(user, userDto.Password);

            if (!result.Succeeded)
            {
                throw new UserException(String.Join($";{Environment.NewLine}", result.Errors));
            }
            await _userUnitOfWork.UserManager.AddToRoleAsync(user.Id, "Client");
            // создаем профиль клиента
            //ClientProfile clientProfile = new ClientProfile { Id = user.Id, Address = userDto.Address, Name = userDto.Name };
            //Database.ClientManager.Create(clientProfile);
            await _userUnitOfWork.CommitAsync();
        }

        public async Task<ClaimsIdentity> Authenticate(UserDto userDto)
        {
            // находим пользователя
            ApplicationUser user = await _userUnitOfWork.UserManager.FindAsync(userDto.Email, userDto.Password);
            // авторизуем его и возвращаем объект ClaimsIdentity
            if (user == null)
            {
                throw new UserException("Invalid email or password");
            }

            return await _userUnitOfWork.UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        private bool _disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _userUnitOfWork.Dispose();
                }
                _disposed = true;
            }
        }
    }
}
