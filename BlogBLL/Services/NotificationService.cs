﻿using AutoMapper;
using BlogBLL.DTOs;
using BlogBLL.Services.Interfaces;
using BlogDAL.Entities;
using BlogDAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogBLL.Services
{
    public class NotificationService : INotificationService
    {
        private readonly IBlogUnitOfWork _blogUnitOfWork;
        private readonly IMapper _mapper;

        public NotificationService(IBlogUnitOfWork blogUnitOfWork, IMapper mapper)
        {
            _blogUnitOfWork = blogUnitOfWork;
            _mapper = mapper;
        }

        public IEnumerable<NotificationDto> GetAll()
        {
            return _blogUnitOfWork.NotificationRepository
                .FindAll()
                .Select(x => _mapper.Map<NotificationDto>(x));
        }

        public async Task<NotificationDto> GetByIdAsync(int id)
        {
            return _mapper.Map<NotificationDto>(await _blogUnitOfWork.NotificationRepository.GetByIdAsync(id));
        }

        public async Task<int> AddAsync(NotificationDto model)
        {
            var notification = _mapper.Map<Notification>(model);
            _blogUnitOfWork.NotificationRepository.Add(notification);
            await _blogUnitOfWork.CommitAsync();

            return notification.Id;
        }

        public async Task UpdateAsync(NotificationDto model)
        {
            _blogUnitOfWork.NotificationRepository.Update(_mapper.Map<Notification>(model));
            await _blogUnitOfWork.CommitAsync();
        }

        public async Task DeleteByIdAsync(int modelId)
        {
            await _blogUnitOfWork.NotificationRepository.DeleteByIdAsync(modelId);
            await _blogUnitOfWork.CommitAsync();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        private bool _disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _blogUnitOfWork.Dispose();
                }
                _disposed = true;
            }
        }
    }
}
