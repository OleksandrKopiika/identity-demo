﻿using BlogBLL.DTOs.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogBLL.Services.Interfaces
{
    public interface ICreateReadDelete<TDto> where TDto : BaseDto 
    {
        IEnumerable<TDto> GetAll();

        Task<TDto> GetByIdAsync(int id);

        Task<int> AddAsync(TDto model);

        Task DeleteByIdAsync(int id);
    }
}
