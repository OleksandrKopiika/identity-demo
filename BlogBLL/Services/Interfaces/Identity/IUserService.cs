﻿using BlogBLL.DTOs.Identity;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace BlogBLL.Services.Interfaces.Identity
{
    public interface IUserService : IDisposable
    {
        Task Create(UserDto userDto);
        Task<ClaimsIdentity> Authenticate(UserDto userDto);
    }
}
