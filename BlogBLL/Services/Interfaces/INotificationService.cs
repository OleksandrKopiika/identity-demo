﻿using BlogBLL.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogBLL.Services.Interfaces
{
    public interface INotificationService : ICreateReadDelete<NotificationDto>, IDisposable
    {
    }
}
