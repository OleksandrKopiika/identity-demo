﻿using BlogBLL.DTOs.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogBLL.DTOs
{
    public class NotificationDto : BaseDto
    {
        public string Text { get; set; }
    }
}
