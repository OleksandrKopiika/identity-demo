﻿using AutoMapper;
using BlogBLL.DTOs;
using BlogDAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogBLL.AutomapperProfiles
{
    public class BlogAutomapperProfile : Profile
    {
        public BlogAutomapperProfile()
        {
            CreateMap<Article, ArticleDto>()
                .ReverseMap();

            CreateMap<Notification, NotificationDto>()
                .ReverseMap();
        }
    }
}
