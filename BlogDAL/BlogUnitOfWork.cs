﻿using BlogDAL.Interfaces;
using BlogDAL.Repositories;
using BlogDAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogDAL
{
    public class BlogUnitOfWork : IBlogUnitOfWork
    {
        private readonly BlogDbContext _context;

        public BlogUnitOfWork(BlogDbContext context)
        {
            _context = context;
            Util.EnsureStaticReference<System.Data.Entity.SqlServer.SqlProviderServices>();
        }

        public IArticleRepository ArticleRepository => new ArticleRepository(_context);

        public INotificationRepository NotificationRepository => new NotificationRepository(_context);

        public async Task<int> CommitAsync() => await _context.SaveChangesAsync();

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        private bool _disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
                _disposed = true;
            }
        }
    }
}
