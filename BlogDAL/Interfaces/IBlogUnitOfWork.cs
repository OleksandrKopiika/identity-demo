﻿using BlogDAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogDAL.Interfaces
{
    public interface IBlogUnitOfWork : IDisposable
    {
        IArticleRepository ArticleRepository { get; }

        INotificationRepository NotificationRepository { get; }

        Task<int> CommitAsync();
    }
}
