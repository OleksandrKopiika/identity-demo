﻿using BlogDAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogDAL.Repositories.Interfaces
{
    public interface IArticleRepository : IBaseRepository<Article>
    {
        IQueryable<Article> GetArticlesByUserId(Guid userId);
     
        Task<Article> GetByIdWithDetailsAsync(int id);
    }
}
