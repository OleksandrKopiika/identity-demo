﻿using BlogDAL.Entities.Abstract;
using BlogDAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogDAL.Repositories
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : BaseEntity
    {
        protected BlogDbContext _context;
        protected DbSet<TEntity> ConcreteDbSet;

        public BaseRepository(BlogDbContext context)
        {
            _context = context;
            ConcreteDbSet = context.Set<TEntity>();
        }
        public IQueryable<TEntity> FindAll() => ConcreteDbSet;

        public async Task<TEntity> GetByIdAsync(int id)
        {
            return await ConcreteDbSet.SingleOrDefaultAsync(x => x.Id == id);
        }

        public void Add(TEntity entity)
        {
            ConcreteDbSet.Add(entity);
        }

        public void Update(TEntity entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
        }

        public void Delete(TEntity entity)
        {
            _context.Entry(entity).State = EntityState.Deleted;
        }

        public async Task DeleteByIdAsync(int id)
        {
            TEntity entityToDelete = (await GetByIdAsync(id))
                ?? throw new ArgumentException($"There is no record for type {typeof(TEntity)} with given id {id}", nameof(id));

            ConcreteDbSet.Remove(entityToDelete);
        }
    }
}
