﻿using BlogDAL.Entities;
using BlogDAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogDAL.Repositories
{
    public class ArticleRepository : BaseRepository<Article>, IArticleRepository
    {
        public ArticleRepository(BlogDbContext context) : base(context)
        {
        }

        public IQueryable<Article> GetArticlesByUserId(Guid userId)
        {
            return ConcreteDbSet;//ConcreteDbSet.Where(x => x.UserId == userId);
        }

        public async Task<Article> GetByIdWithDetailsAsync(int id)
        {
            return await ConcreteDbSet.SingleOrDefaultAsync(x => x.Id == id);
        }
    }
}
