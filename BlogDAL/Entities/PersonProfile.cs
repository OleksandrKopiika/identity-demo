﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogDAL.Entities
{
    public class PersonProfile
    {
        [Key]
        [ForeignKey(nameof(Person))]
        public int PersonId { get; set; }

        public string Bio { get; set; }

        public string Slug { get; set; }

        public virtual Person Person { get; set; }
    }
}
