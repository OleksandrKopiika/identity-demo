﻿using BlogDAL.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogDAL.Entities
{
    public class Notification : BaseEntity
    {
        public string Text { get; set; }
    }
}
