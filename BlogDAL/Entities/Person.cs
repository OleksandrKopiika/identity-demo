﻿using BlogDAL.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogDAL.Entities
{
    public class Person : BaseEntity
    {
        public string ApplicationUserId { get; set; }

        public string Nickname { get; set; }

        public virtual PersonProfile Profile { get; set; }
    }
}
