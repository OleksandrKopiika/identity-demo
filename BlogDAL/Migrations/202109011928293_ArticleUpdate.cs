﻿namespace BlogDAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ArticleUpdate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Articles", "Slug", c => c.String());
            AddColumn("dbo.Articles", "CreatedAt", c => c.DateTime(nullable: false));
            AddColumn("dbo.Articles", "UpdatedAt", c => c.DateTime(nullable: false));
            DropColumn("dbo.Articles", "Published");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Articles", "Published", c => c.DateTime(nullable: false));
            DropColumn("dbo.Articles", "UpdatedAt");
            DropColumn("dbo.Articles", "CreatedAt");
            DropColumn("dbo.Articles", "Slug");
        }
    }
}
