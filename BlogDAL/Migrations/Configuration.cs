﻿namespace BlogDAL.Migrations
{
    using BlogDAL.Entities;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<BlogDAL.BlogDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(BlogDAL.BlogDbContext context)
        {
            var articles = new List<Article>
            {
                new Article{ Text = "super Text", Title = "nice", CreatedAt = new DateTime(2013, 5, 20), UpdatedAt = new DateTime(2013, 5, 20), Slug = "super-text"},
                new Article{ Text = "Awesome Text", Title = "beautiful", CreatedAt = new DateTime(2019, 3, 26), UpdatedAt = new DateTime(2019, 3, 26), Slug = "awesome-text"},
                new Article{ Text = "Gorgeous Text", Title = "careful", CreatedAt = new DateTime(2021, 8, 23), UpdatedAt = new DateTime(2021, 9, 2), Slug = "gorgeous-text"}
            };
            articles.ForEach(s => context.Articles.AddOrUpdate(p => p.Title, s));
            context.SaveChanges();

        }
    }
}
