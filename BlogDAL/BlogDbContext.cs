﻿using BlogDAL.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogDAL
{
    public class BlogDbContext : DbContext
    {
        public BlogDbContext() : base("BlogContext")
        {
        }

        public DbSet<Article> Articles { get; set; }
        public DbSet<Notification> Notifications { get; set; }
    }
}
