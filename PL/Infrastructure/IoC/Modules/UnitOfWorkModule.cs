﻿using BlogDAL;
using BlogDAL.Interfaces;
using IdentityDAL;
using IdentityDAL.Interfaces;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PL.Infrastructure.IoC.Modules
{
    public class UnitOfWorkModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IBlogUnitOfWork>().To<BlogUnitOfWork>();
            Bind<IUserUnitOfWork>().To<UserUnitOfWork>();
        }
    }
}