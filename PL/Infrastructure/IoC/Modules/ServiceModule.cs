﻿using BlogBLL.Services;
using BlogBLL.Services.Identity;
using BlogBLL.Services.Interfaces;
using BlogBLL.Services.Interfaces.Identity;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PL.Infrastructure.IoC.Modules
{
    public class ServiceModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IArticleService>().To<ArticleService>();
            Bind<INotificationService>().To<NotificationService>();

            Bind<IUserService>().To<UserService>();
        }
    }
}