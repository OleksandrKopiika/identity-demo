﻿using AutoMapper;
using BlogBLL.DTOs;
using BlogBLL.DTOs.Identity;
using PL.Models.Account;
using PL.Models.Articles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PL.Infrastructure.AutomapperProfiles
{
    public class DtoViewModelAutomapperProfile : Profile
    {
        public DtoViewModelAutomapperProfile()
        {
            CreateMap<CreateArticleViewModel, ArticleDto>();

            CreateMap<UpdateArticleViewModel, ArticleDto>()
                .ReverseMap();

            CreateMap<ArticleDto, ArticleDetailsViewModel>();

            CreateMap<ArticleDto, ArticleSummaryViewModel>()
                .ForMember(dest => dest.TimeFromLastUpdate, opt => opt.MapFrom(src => DateTime.Now - src.UpdatedAt ));

            CreateMap<LoginViewModel, UserDto>();
            CreateMap<RegisterViewModel, UserDto>();
        }
    }
}