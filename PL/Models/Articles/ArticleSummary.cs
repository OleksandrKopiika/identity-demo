﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PL.Models.Articles
{
    public class ArticleSummaryViewModel
    {
        public int Id { get; set; }
        public string Slug { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public TimeSpan TimeFromLastUpdate { get; set; }
    }
}