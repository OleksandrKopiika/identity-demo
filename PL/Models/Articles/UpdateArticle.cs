﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PL.Models.Articles
{
    public class UpdateArticleViewModel : EditArticleBase
    {
        public int Id { get; set; }
    }
}