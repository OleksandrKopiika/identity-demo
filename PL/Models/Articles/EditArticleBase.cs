﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PL.Models.Articles
{
    public class EditArticleBase
    {
        [Required, StringLength(128)]
        public string Title { get; set; }

        [Required, StringLength(4096)]
        public string Text { get; set; }
    }
}