﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PL.Models.Articles
{
    public class ArticleDetailsViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}