﻿using AutoMapper;
using BlogBLL.DTOs.Identity;
using BlogBLL.Exceptions;
using BlogBLL.Services.Interfaces.Identity;
using Microsoft.Owin.Security;
using PL.Models.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace PL.Controllers
{
    public class AccountController : Controller
    {
        private readonly IUserService _service;
        private readonly IMapper _mapper;

        public AccountController(IUserService service, IMapper mapper) => (_service, _mapper) = (service, mapper);

        private IAuthenticationManager AuthenticationManager
        {
            get => HttpContext.GetOwinContext().Authentication;
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = _mapper.Map<UserDto>(model);

                try
                {
                    await Authenticate(user);
                    return RedirectToAction("Index", "Home");
                }
                catch (UserException ex)
                {
                    ModelState.AddModelError(String.Empty, ex.Message);
                }
                catch (Exception)
                {
                    ModelState.AddModelError(String.Empty, "Some mistakes occured while authentication");
                }
                
            }
            return View(model);
        }

        public ActionResult Logout()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = _mapper.Map<UserDto>(model);

                try
                {
                    await _service.Create(user);
                    await Authenticate(user);
                }
                catch (UserException ex)
                {
                    ModelState.AddModelError(String.Empty, ex.Message);
                }
            }

            return View(model);
        }

        private async Task Authenticate(UserDto user)
        {
            ClaimsIdentity claim = await _service.Authenticate(user);

            AuthenticationManager.SignOut();
            AuthenticationManager.SignIn(new AuthenticationProperties
            {
                IsPersistent = true
            }, claim);
        }
    }
}