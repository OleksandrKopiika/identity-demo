﻿using AutoMapper;
using BlogBLL.DTOs;
using BlogBLL.Services.Interfaces;
using PL.Models.Articles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace PL.Controllers
{
    public class ArticlesController : Controller
    {
        private readonly IArticleService _service;
        private readonly IMapper _mapper;

        public ArticlesController(IArticleService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        public ActionResult Index()
        {
            var articles = _service.GetAll();

            return View(_mapper.Map<IEnumerable<ArticleDto>, IEnumerable<ArticleSummaryViewModel>>(articles));
        }

        public async Task<ActionResult> Read(int id, string slug)
        {
            var article = await _service.GetByIdAsync(id);

            if (article.Slug != slug)
            {
                return RedirectToRoute(new { id = id, slug = article.Slug });
            }

            return View(_mapper.Map<ArticleDetailsViewModel>(article));
        }

        [Authorize]
        public ActionResult Create()
        {
            return View(new CreateArticleViewModel());
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult> Create(CreateArticleViewModel input)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var article = _mapper.Map<ArticleDto>(input);
                    var id = await _service.AddAsync(article);

                    return RedirectToAction(nameof(Read), new { id = id });
                }
            }
            catch (Exception)
            {
                // TODO: Log error
                // Add a model-level error by using an empty string key
                ModelState.AddModelError(
                    string.Empty,
                    "An error occured saving the review"
                    );
            }

            //If we got to here, something went wrong
            return View();
        }

        public async Task<ActionResult> Edit(int id)
        {
            return View(_mapper.Map<UpdateArticleViewModel>(await _service.GetByIdAsync(id)));
        }

        [HttpPost]
        public async Task<ActionResult> Edit(UpdateArticleViewModel input)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var article = _mapper.Map<ArticleDto>(input);
                    await _service.UpdateAsync(article);

                    return RedirectToAction(nameof(Read), new { id = article.Id });
                }
            }
            catch (Exception)
            {
                // TODO: Log error
                // Add a model-level error by using an empty string key
                ModelState.AddModelError(
                    string.Empty,
                    "An error occured saving the review"
                    );
            }

            //If we got to here, something went wrong
            return View();
        }
    }
}